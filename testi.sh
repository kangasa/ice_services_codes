#!/bin/bash
DATE_TODAY=`date +"%Y%m%d"`
TIME=`date +"%T"`
echo $DATE_TODAY $TIME

FILE=/data/mep/balticsea/baltic/icechart/pdf/color/IceChart_${DATE_TODAY}_Baltic.pdf
#FILE=IceChart_${DATE_TODAY}_Baltic.pdf

if [ ! -f "$FILE" ]
then
    echo "Taman paivan pdf jaakarttaa ei ole viela siirretty NAS:iin. Kello on nyt $TIME ja toimituksen takaraja 15:20. Tama viesti on lahetetty iceservice(at)oper-ice.fmi.fi crontabista." | mail -s "Jaakartan toimituksen takaraja 15:20 lahestyy!" ice.service.production.admin@fmi.fi
    echo "File $FILE does not exist !!!"
    exit
else
    echo "File $FILE exists"
fi

SIZE=$(du -sb $FILE | awk '{ print $1 }')
if ((SIZE<500000)) ; then
    echo "Taman paivan jaakartan ( IceChart_${DATE_TODAY}_Baltic.pdf ) koko on epailyttavan pieni! Koko on vain $SIZE tavua, kun normaali tiedopstokoko on 1-2Mt. Tarkista onko kartta kunnossa. Tama viesti on lahetetty iceservice(at)oper-ice.fmi.fi crontabista" | mail -s "Jaakartan koko epailyttavan pieni!" ice.service.production.admin@fmi.fi
    echo "Jaakartan koko liian pieni?"
    echo $SIZE
else
    echo "Check completed succesfully. Ice chart delivered to NAS and larger than treshold limit.";
fi

exit
